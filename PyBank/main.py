import os
import csv 


# analyzes the records to calculate each of the following:
# The total number of months included in the dataset
# The net total amount of "Profit/Losses" over the entire period
# The average of the changes in "Profit/Losses" over the entire period
# The greatest increase in profits (date and amount) over the entire period
# The greatest decrease in losses (date and amount) over the entire period
budget_csv = os.path.join("Resources", "budget_data.csv")

with open(budget_csv) as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",")
    num_months = 0
    net_total = 0
    greatest_increase = 0
    greatest_decrease = 0
    greatest_increase_month = ''
    greatest_decrease_month = ''
    #skip header row
    next(csvreader)

    for row in csvreader:
        #increment month counter
        num_months += 1
        month_year = row[0]
        #add profit/loss to net total
        profit_loss = int(row[1])
        net_total += profit_loss
        #check for greatest profit or loss
        if(profit_loss > 0):
            #increase
            if(profit_loss > greatest_increase):
                greatest_increase = profit_loss
                greatest_increase_month = month_year
        if(profit_loss < 0):
            #decrease
            if(profit_loss < greatest_decrease):
                greatest_decrease = profit_loss
                greatest_decrease_month = month_year

    avg = net_total / num_months

analysis = (
    f"Financial Analysis\n"
    f"----------------------------\n"
    f"Total Months: {num_months}\n"
    f"Total: ${net_total}\n"
    f"Average Change: ${'{:.2f}'.format(avg)}\n"
    f"Greatest Increase in Profits: {greatest_increase_month} (${greatest_increase})\n"
    f"Greatest Decrease in Profits: {greatest_decrease_month} (${greatest_decrease})\n"
)

#create and write analysis text file
file_obj = open(os.path.join("Analysis", "analysis.txt"),"w") 
file_obj.write(analysis)
file_obj.close()

#output analysis
print(analysis)




