import os
import csv 


# analyzes the votes and calculates each of the following:
# The total number of votes cast
# A complete list of candidates who received votes
# The percentage of votes each candidate won
# The total number of votes each candidate won
# The winner of the election based on popular vote.

votes_csv = os.path.join("Resources", "election_data.csv")

with open(votes_csv) as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",")
    
    num_votes = 0
    candidate_votes = {}
    candidate_percentage = {}

    #skip header row
    next(csvreader)

    votes_data = [str(x[2]) for x in csvreader if str(x[2]) != 'Candidate']
    
    
    # traverse for unique names
    unique_candidates = []
    for x in votes_data: 
        # check if exists in unique_list or not 
        if x not in unique_candidates: 
            unique_candidates.append(x) 

    # initialize vote counter object for each unique candidate
    for x in unique_candidates:
        candidate_votes[x] = 0
    
    # count up all the votes
    for candidate in votes_data:
        num_votes += 1
        candidate_votes[candidate] += 1

    # calculate percentage for each candidate
    for name in candidate_votes:
        percentage =  candidate_votes[name] / num_votes * 100
        candidate_percentage[name] = percentage

    

    winner = ''
    greatest_votes = 0

    votes_list = []
    for name in candidate_votes:
        #add to list for tie check later
        votes_list.append(candidate_votes[name])
        if candidate_votes[name] > greatest_votes:
            greatest_votes = candidate_votes[name]
            winner = name

    # #uncomment to test if tie checker works
    # votes_list.append(greatest_votes)
    # candidate_votes["Mike"] = greatest_votes
    # candidate_percentage["Mike"] = candidate_percentage[winner]

    #check if there was a tie
    if len(votes_list) != len(set(votes_list)):
        #there are duplicate vote scores, check if the winning score is duplicated
        occurrance = 0
        for count in votes_list:
            if count == greatest_votes:
                occurrance += 1
        if occurrance > 1:
            winner = "Oh no, We have a tie!"
    
    # show results for each candidate 
    analysis = (
        f"Election Results\n"
        "----------------------------\n"
        f"Total Votes: {num_votes}\n"
        "----------------------------\n"
    )
    for name in candidate_votes:
        analysis += f"{name}: {'{:.4f}'.format(candidate_percentage[name])}% {candidate_votes[name]}\n"
    
    #finish the analysis string
    analysis += "----------------------------\n"
    analysis += f"Winner: {winner}\n"

#create and write analysis text file
file_obj = open(os.path.join("Analysis", "analysis.txt"),"w") 
file_obj.write(analysis)
file_obj.close()

#output analysis
print(analysis)
    